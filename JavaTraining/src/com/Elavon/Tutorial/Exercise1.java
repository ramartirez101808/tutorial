package com.Elavon.Tutorial;

public class Exercise1 {

	public static void main(String[] args) {
		//print my name in asterisk
		System.out.println("******	   *    *       *        **        *        *         *    *        *");
		System.out.println("*     *	   *    * *     *      *    *      *          *      *     * *      *");
		System.out.println("* ***	   *    *   *   *       *****      *             *         *    *   *");
		System.out.println("*    *	   *    *     * *    *        *    *             *         *      * *");
		System.out.println("*      *	   *    *       *    *        *    ******        *          *       *");
		System.out.println("\n");
		System.out.println("\n");
		System.out.println("*       *     * *     ******    ******    *     ******      ******    ******");
		System.out.println("* *   * *    *   *    *     *      *      *     *     *     *             *");
		System.out.println("*   *   *   ******    * ***        *      *     * ***       ****        *");
		System.out.println("*       *   *    *    *    *       *      *     *    *      *          *");
		System.out.println("*       *   *    *    *      *     *      *     *       *   ******    ******");
	}

}
